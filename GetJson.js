var http = require("http"),
fs = require('fs'),
url = require('url'),
querystring = require('querystring');

var express = require("express"),
app = express(),
upload = require("express");

//SignUp page
var options = {
    host: "localhost:3003/",
    path: "/SignUp",
    method: "POST",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer token"
    }
};

var User = {};
User.name = "peter";
User.Email = "peter@gmail.com";
User.Password = "abcd123456";

var options = {
    host: "localhost:3003/",
    path: "/SignUp" + User.name + User.Email + User.Password,
    method: "PUT", 
    headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": "Bearer token"
    }
};

var options = {
    host: 'localhost:3003/',
    port: 200,
    path: '/SignUp',
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
};

//FoodMenu page
var options = {
    host: "localhost:3003/",
    path: "/FoodMenu",
    method: "POST",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer token"
    }
};

var photo = {};
photo.filename = "0029b7ccaf4b01936f99c404fedd4f4d.jpg";

var options = {
    host: "localhost:3003/",
    path: "/FoodMenu" + photo.filename,
    method: "PUT", 
    headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": "Bearer token"
    }
};

//FoodData page
var options = {
    host: "localhost:3003/",
    path: "/content",
    method: "POST",
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer token"
    }
};

var FoodRecord = {};
FoodRecord.ID = "1";
FoodRecord.Name = "Pizza";
FoodRecord.Price = "$56";
FoodRecord.Type = "Food";

var options = {
    host: "localhost:3003/",
    path: "/content" + FoodRecord.ID + FoodRecord.Name + FoodRecord.Price + FoodRecord.Type,
    method: "PUT",
    headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": "Bearer token"
    }
};

var options = {
    host: 'localhost:3003/',
    port: 200,
    path: '/content',
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
};

var content = fs.readFileSync('SignUpData.json','UTF-8');
console.log(content);

var content1 = fs.readFileSync('UploadPhotoData.json','UTF-8');
console.log(content1);

var content2 = fs.readFileSync('FoodData.json','UTF-8');
console.log(content2);

app.use(upload())
console.log("Server running at http://127.0.0.1:3003/");
app.get("/FoodMenu", function(req, res){
    res.sendFile(_dirname+"/FoodMenu");
});

app.post("/FoodMenu", function(req, res){
    if(req.files){
        var file = req.files.filename, filename = file.name;
        file.mv("./upload/"+filename, function(err){
            if(err){
                console.log(err)
                res.send("error occured")
            }else{
                res.send("Done!")
            }
        });
    }
});

app.delete('/content/:ID', function(req, res){
    console.log('Deleted', req.body);
});

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type':'text/html'});
    if (req.url === "/home"){
        fs.createReadStream("./Home.html","UTF-8").pipe(res);
    }

    if (req.url === "/"){
        fs.createReadStream("./SignUp.html","UTF-8").pipe(res);
    }

    if (req.url === "/Login"){
        fs.createReadStream("./Login.html","UTF-8").pipe(res);
    }

    if (req.url === "/FoodMenu"){
        fs.createReadStream("./FoodMenu.html","UTF-8").pipe(res);
    }

    if (req.url === "/content"){
        fs.createReadStream("./content.html","UTF-8").pipe(res);
    }

    if (req.method === "GET"){
        var q = url.parse(req.url, true).query;
        console.log(q);
    } else if (req.method === "POST"){
        var data = "";
        req.on("data", function(chunk){
            data += chunk;
        });

        req.on("end", function(chunk){
            var userdata = querystring.parse(data);
            console.log(userdata);
        });
    }
}).listen(3003);